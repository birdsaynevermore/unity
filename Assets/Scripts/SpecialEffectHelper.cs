﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialEffectHelper : MonoBehaviour

{


    public static SpecialEffectHelper Instance;

    public ParticleSystem SmokeEffect;
    public ParticleSystem BurnEffect;


    private void Awake()
    {
         if(Instance != null)
        {
            Debug.LogError("Несколько экземпляров SpecialEffectHelper!");
        }

        Instance = this;
    }

    public void Explosion(Vector3 position)
    {
        Instantiate(SmokeEffect, position);

        Instantiate(BurnEffect, position);
    }

    private ParticleSystem Instantiate (ParticleSystem prefab, Vector3 position)
    {
        ParticleSystem newParticleSystem = Instantiate(prefab, position, Quaternion.identity) as ParticleSystem;

        Destroy(newParticleSystem.gameObject, newParticleSystem.main.startLifetimeMultiplier);

        return newParticleSystem;
    }



}
   