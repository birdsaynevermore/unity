﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour
{

    public static GameMenu Instance;

    private void Start()
    {
        Instance = this; 
    }


    public GameObject DeathPanel;

    public void OnStartButtonClick()
    {
        SceneManager.LoadScene("Stage1");
    }

    public void OnExitButtonClick2()
    {
        Application.Quit();
    }

    public void OnStartButtonClickBackToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public Text textScore;

    public Text Health;
    

   

}
