﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{

    public Vector2 speed = new Vector2(50, 50);
    private Vector2 movement;
    private Rigidbody2D rb;

    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }


    void Update()
    {

        bool shoot = Input.GetButtonDown("Fire1");
        

        if (shoot) { 
       
            WeaponScript weapon = GetComponent<WeaponScript>();
            if (weapon != null && weapon.CanAttack)
            { 
                weapon.Attack(false);
                SoundEffectHelper.Instance.MakePlayerShotSound();
            }

        }

        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");

        movement = new Vector2(speed.x * Time.deltaTime * inputX, speed.y * Time.deltaTime * inputY);

        rb.velocity = movement;

       
    }
}
