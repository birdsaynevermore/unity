﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{

    private WeaponScript [] weapons;

    public GameObject[] enemies;
    public Vector3 SpawnValue;
    public float spawnWait;
    public float spawnMostWait;
    public float spawnLeastWait;
    public int startWait;
    public bool stop;

    int randEnemy;

    private void Awake()
    {
        weapons = GetComponentsInChildren<WeaponScript>();
    }

    private void Start ()
    {

        StartCoroutine(waitSpawner());

    }

   

    private void Update ()
    {

        spawnWait = Random.Range(spawnLeastWait, spawnMostWait);

        foreach(WeaponScript weapon in weapons){
            if (weapons != null && weapon.CanAttack)
            {
                weapon.Attack(true);
                SoundEffectHelper.Instance.MakeEnemyShotSound();
            }
        }
	}
    
    IEnumerator waitSpawner()
    {
        yield return new WaitForSeconds(startWait);

        while (!stop)
        {
            randEnemy = Random.Range(0, enemies.Length);

            Vector3 spawnPosition = new Vector3(Random.Range(-SpawnValue.x, SpawnValue.x), Random.Range(-SpawnValue.y, SpawnValue.y), 1);
            Instantiate(enemies[randEnemy], spawnPosition + transform.TransformPoint(0, 0, 0), gameObject.transform.rotation);

            yield return new WaitForSeconds(spawnWait);
        }
    }
}
