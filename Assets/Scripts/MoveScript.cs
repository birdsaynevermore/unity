﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveScript : MonoBehaviour
{

    public Vector2 speed = new Vector2(5, 5);

    public Vector2 direction = new Vector2(-1, 0);

    public Vector2 movement;

    public Rigidbody2D rb;
   

	private void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
	}
	

	
	private void Update ()
    {

        movement = new Vector2
            (speed.x * Time.deltaTime * direction.x,
            speed.y * Time.deltaTime * direction.y);
		
	}

    void FixedUpdate()
    {
        rb.velocity = movement;
    }
}
