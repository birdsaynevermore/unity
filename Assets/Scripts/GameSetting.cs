﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSetting : MonoBehaviour
{


    public static GameSetting Instance;

    private void Start()
    {
        Instance = this;
    }

    public int score = 0;

    public int Score
    {
        get
        {
            return score;
        }

        set
        {

            score = value;
            GameMenu.Instance.textScore.text = score.ToString();



        }
    }

    

}
