﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scrolling : MonoBehaviour
{

    public Vector2 speed = new Vector2(2, 2);

    public Vector2 direction = new Vector2(0, -1);

    public bool isLinkedCamera = false;
	
	private void Start () {
		
	}
	
	
	private void Update ()
    {

        Vector3 movment = new Vector3(
            speed.x * Time.deltaTime * direction.x,
            speed.y * Time.deltaTime * direction.y, 0);

            transform.Translate(movment);

        if (isLinkedCamera)
        {
            Camera.main.transform.Translate(movment);
        }
		
	}
}
