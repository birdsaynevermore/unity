﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponScript : MonoBehaviour

{

    public Transform shotPrefab;

    
    public float shootingRate = 0.25f;

    private float shootCooldown;




    private void Start()
    {

        shootCooldown = 0f;

    }


    public void Attack(bool isEnemy)
    {
        if (CanAttack)
        {
            shootCooldown = shootingRate;


            // new shot
            var shotTransform = Instantiate(shotPrefab) as Transform;

           

            //position
            shotTransform.position = transform.position;

            //Enemy 

            ShotScript shot = shotTransform.gameObject.GetComponent<ShotScript>();
            if (shot != null)
            {
                shot.isEnemyShot = isEnemy;
            }

            MoveScript move = shotTransform.gameObject.GetComponent<MoveScript>();
            if (move != null)
            {
                move.direction = this.transform.right; //right to sprite
            }
        }
    }
            

            /// <summary>
            /// IF ready to shot
            /// </summary>

            public bool CanAttack
    {
        get
        {
            return shootCooldown <= 0f;
        }
    }
  
	
	private void Update ()
    {
		
        if(shootCooldown > 0)
        {
            shootCooldown -= Time.deltaTime;
        }
	}
}