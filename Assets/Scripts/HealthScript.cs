﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthScript : MonoBehaviour {

    public int hp;
    public bool isEnemy = true;

    

    /// <summary>
    ///  
    /// </summary>
    /// <param name="damageCount"></param>
    public void Damage(int damageCount)
    {

        hp -= damageCount;

        if(GetComponent<PlayerBehaviour>() != null)
        {
            GameMenu.Instance.Health.text = hp.ToString();
        }

        

        if (hp <= 0)
        {
            SpecialEffectHelper.Instance.Explosion(transform.position);
            SoundEffectHelper.Instance.MakeExplosionSound();
            //death

           

            if (GetComponent<PlayerBehaviour>() != null)
            {
                GameMenu.Instance.DeathPanel.SetActive(true);
                Camera.main.transform.parent = null;
                
                
                
              
            }
            
            

            else
            {

                GameSetting.Instance.Score++;

            }

            
            
            Destroy(gameObject);
            
            



        }
    }

    private void OnTriggerEnter2D(Collider2D otherCollider)
    {
        ShotScript shot = otherCollider.gameObject.GetComponent<ShotScript>();
        if (shot != null)
        {
            if (shot.isEnemyShot != isEnemy)
            {
                Damage(shot.damage);

                Destroy(shot.gameObject);


            }
        }
    }
}
