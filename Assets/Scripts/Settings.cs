﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{

 
    public AudioMixer am;
   
    bool isFullScreen;

    public void FullScreenToggle()
    {
        isFullScreen = !isFullScreen;
        Screen.fullScreen = isFullScreen;

      
    }
    

    public void AudioVolume (float sliderValue)
    {
        am.SetFloat("masterVloume", sliderValue);
    }

    public void Quality(int q)
    {
        QualitySettings.SetQualityLevel(q);
       
    }

    Resolution[] rsl;
    List<string> resolutions;
    public Dropdown dropdown;

    public void Awake()
    {

        resolutions = new List<string>();
        rsl = Screen.resolutions;
        foreach (var i in rsl)
        {
            resolutions.Add(i.width + "x" + i.height);
            
        }

        dropdown.ClearOptions();
        dropdown.AddOptions(resolutions);
       
        

    }

    public void Resolutions(int r)
    {
        Screen.SetResolution(rsl[r].width, rsl[r].height, isFullScreen);
        
    }
    
    
    
}