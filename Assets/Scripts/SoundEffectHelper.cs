﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectHelper : MonoBehaviour
{

    public AudioSource audioSource;
    public static SoundEffectHelper Instance;
    public AudioClip explosionSound;
    public AudioClip playerShotSound;
    public AudioClip enemyShotSound;

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("Несколько экземпляров SoundEffectHelper!");
        }

        Instance = this;
    }

    public void MakeExplosionSound()
    {
        MakeSound(explosionSound);

    }
    public void MakeEnemyShotSound()
    {
        MakeSound(enemyShotSound);
    }

    public void MakePlayerShotSound()
    {
        MakeSound(playerShotSound);
    }

    private void MakeSound(AudioClip originalClip)
    {
        audioSource.clip = originalClip;
        audioSource.Play();
        
    }
}
 